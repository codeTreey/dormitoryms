package com.laoye;


import org.junit.jupiter.api.Test;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

public class Base64Test {

    @Test
    public void testBase64() throws UnsupportedEncodingException {
        String encode = Base64.getEncoder().encodeToString("test".getBytes("UTF-8"));
        System.out.println(encode);
        // 解码
        byte[] decode = Base64.getDecoder().decode(encode);
        System.out.println(new String(decode, "UTF-8"));
    }


}
